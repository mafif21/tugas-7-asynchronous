var readBooksPromise = require("./promise.js");

var books = [
  { name: "LOTR", timeSpent: 3000 },
  { name: "Fidas", timeSpent: 2000 },
  { name: "Kalkulus", timeSpent: 4000 },
  { name: "komik", timeSpent: 1000 },
];

// Lanjutkan code untuk menjalankan function readBooksPromise
var waktuAwal = 10000;
readBooksPromise(waktuAwal, books[0]).then(sisa => {
  readBooksPromise(sisa, books[1]).then(sisa2 => {
    readBooksPromise(sisa2, books[2]).then(sisa3 => {
      readBooksPromise(sisa3, books[3]).catch(sisa => {
        console.log(sisa);
      });
    }).catch;
  }).catch;
}).catch;
